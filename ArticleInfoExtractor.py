from newspaper import Article, Config

class ArticleInfoExtractor:
    """ Simple class for an article containing only the title and the text,
    which are both retrieved using the package newspaper3k from a URL.
    Both can be kept in the original language or translated by specifying translation"""

    def __init__(self, url, translation=False):
        self.title, self.text = self.process_url(url, translation)

    def process_url(self, url, translation):
        HEADERS = {
          "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0",
          "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
          "Accept-Language": "en-US,en;q=0.5",
          "Accept-Encoding": "gzip, deflate",
          "Connection": "keep-alive",
          "Upgrade-Insecure-Requests": "1",
          "Sec-Fetch-Dest": "document",
          "Sec-Fetch-Mode": "navigate",
          "Sec-Fetch-Site": "none",
          "Sec-Fetch-User": "?1",
          "Cache-Control": "max-age=0",
        }

        config = Config()
        config.headers = HEADERS
        config.request_timeout = 10

        # Some news websites might block the download of the article performed
        # by newspaper3k even with suitable HEADERS, raising an error
        try:
          article = Article(url)
          article.download()
          article.parse()
        except Exception as e:
          print("Article could not be downloaded")
          print(e)

        if translation:
          text  = ts.google(article.text, to_language='en')
          title = ts.google(article.title, to_language='en')
        else:
          text  = article.text
          title = article.title

        return title, text
