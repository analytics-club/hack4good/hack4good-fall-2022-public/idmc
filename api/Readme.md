## Prerequisites
Need to have docker installed.

## How to Run
Open the terminal and do:
```
./cli.sh
```
The select `b`, this will build and run the docker container. Visit `http://localhost:8080/docs` to access the OpenAPI docs and try the endpoints. **NOTE**: this is still in development mode. 

## Architecture

<img src="images/draft_architecture.png" width="1000" height="500" />