#!/bin/sh

# Used to export all env variables in .env file
set -a 
. ./.env
set +a

if [ -z $PORT ]; then
    PORT="8080"
fi

export PORT


gunicorn -c gunicorn.py  wsgi:server