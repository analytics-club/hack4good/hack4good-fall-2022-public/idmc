#!/bin/sh


if [ -z $PORT ]; then
    PORT="8080"
fi

echo $PORT
export PORT

echo "Check and perform migration"

gunicorn -c gunicorn.py wsgi:server 
#gunicorn -c gunicorn.py  wsgi:server > /log/logs.json 2>&1

#gunicorn -c gunicorn.py --log-config gunicorn_logging.conf wsgi:server > /log/logs.json 2>&1