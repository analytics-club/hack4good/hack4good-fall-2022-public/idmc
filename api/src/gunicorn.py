import os
import uvicorn
PORT = os.environ['PORT']

bind = f"0.0.0.0:{PORT}"
worker_class = "uvicorn.workers.UvicornWorker"
workers = 1
timeout = 90
max_requests = 1