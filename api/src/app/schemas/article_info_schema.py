from pydantic import BaseModel
from typing import List, Optional, Dict

## NOTE: This is just an example, change it to whatever is the schema that you want to follow.

class NumericalInfo(BaseModel):
    number_deaths: Optional[int]
    number_displaced: Optional[int]

class GeographicalInfo(BaseModel):
    state: Optional[str]
    ciy: Optional[str]


class EventInfo(BaseModel):
    event_type: Optional[str]
    description: Optional[str]


class ArticleInfo(BaseModel):
    day: str
    numerical_data: NumericalInfo
    geographical_data: Dict 
    url: str
    event: EventInfo



