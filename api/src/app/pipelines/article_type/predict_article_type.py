from app.schemas.article_info_schema import ArticleInfo
from typing import List


def classify_article_type(articles: List[ArticleInfo], ml_model_article_type)->List[ArticleInfo]:
    """
    Use ML model to classify category of article and set them as attribute of the class.
    """
    print("Applying article type model")
    return articles