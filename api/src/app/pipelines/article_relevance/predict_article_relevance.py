from app.schemas.article_info_schema import ArticleInfo
from typing import List


def classify_article_relevance(articles: List[ArticleInfo], ml_model_article_relevance)->List[ArticleInfo]:
    """
    Use ML model to classify if the article is relevant
    """
    print("Applying article relevance model")
    return articles