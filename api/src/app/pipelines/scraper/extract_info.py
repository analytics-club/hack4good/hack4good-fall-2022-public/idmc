from app.schemas.article_info_schema import ArticleInfo
from typing import List

def extract_info(articles: List[ArticleInfo])->List[ArticleInfo]:
    """
    Extract all needed info.
    """
    return articles