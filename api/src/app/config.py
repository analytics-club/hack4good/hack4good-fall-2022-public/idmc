import logging
import os
import pathlib
#import joblib
from pydantic import BaseSettings
from functools import lru_cache

log = logging.getLogger("uvicorn")


class Settings(BaseSettings):
    BASE_DIR: pathlib.Path = pathlib.Path(__file__).parent.parent
    environment: str = os.getenv("ENVIRONMENT", "dev")
    testing: bool = os.getenv("TESTING", 0)
    ml_model_article_relevance = "<load the model from DB>" # To implement
    ml_model_article_type= "<load the model from DB>" # To Implemente


@lru_cache()
def get_settings() -> BaseSettings:
    log.info("Loading config settings from the environment...")
    return Settings()

settings = get_settings()