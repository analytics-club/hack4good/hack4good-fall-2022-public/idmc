from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pathlib import Path
from app.api.api_v1.api import api_router

origins = [
    "http://localhost:8001",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000"
]

ROOT = Path(__file__).resolve().parent.parent

def create_application() -> FastAPI:
    application = FastAPI()
    application.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    application.include_router(api_router, prefix='/api/v1')
    
    return application
