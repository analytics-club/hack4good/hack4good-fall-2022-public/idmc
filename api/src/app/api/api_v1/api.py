from fastapi import APIRouter

from app.api.api_v1.endpoints import view
from app.api.api_v1.endpoints import status
from app.api.api_v1.endpoints import scraper

api_router = APIRouter()


api_router.include_router(view.router, prefix="/view")
api_router.include_router(status.router, prefix="/status")
api_router.include_router(scraper.router, prefix="/scraper")
