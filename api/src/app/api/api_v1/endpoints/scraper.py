from fastapi import APIRouter, HTTPException, Depends
from typing import Dict, List
from app.dao.get_GDELT_urls import get_GDELT_urls
from app.pipelines.scraper.data_extraction import extract_info_from_url
from app.pipelines.article_relevance.predict_article_relevance import classify_article_relevance
from app.pipelines.article_type.predict_article_type import classify_article_type
from app.pipelines.scraper.extract_info import extract_info
from app.dao.DB_crud import save_in_DB
from app.schemas.article_info_schema import ArticleInfo
from app.config import Settings, get_settings
router = APIRouter()


@router.get("/", tags=["start the scraper"])
async def get_events(settings: Settings = Depends(get_settings)) -> Dict:

    """
    - get article from GDELT
    - extract article and other info with newspaper3k
    - classifiy article relevant or not
    - classify article category
    - Extract numerical and geographical info
    - Save to DB
    """
    # probably need to pass the date
    # The whole scraper can also be implemented using a class based approach
    urls: List[str] = get_GDELT_urls()
    articles_info: List[ArticleInfo] = extract_info_from_url(urls)
    articles_info: List[ArticleInfo] = classify_article_relevance(articles_info, settings.ml_model_article_relevance)
    articles_info: List[ArticleInfo] = classify_article_type(articles_info, settings.ml_model_article_type)
    articles_info: List[ArticleInfo] = extract_info(articles_info)
    try:
        save_in_DB(articles_info)
    except BaseException as e:
        raise HTTPException(status_code=500, detail=f"There was a problem saving the file")

    return {
        "message": "The scraper finished processing"
    }