from fastapi import APIRouter, Depends, Query, Depends, HTTPException
from typing import List, Optional, Union, Dict

router = APIRouter()

import json


@router.get("/", tags=["get all the events"])
async def get_events() -> Dict:
    """
    Read data from DB and give them back
    """
    try:
        with open('app/data/events_summary.json', 'r') as f:
            daily_events = json.load(f)
    except BaseException as e:
        raise HTTPException(status_code=500, detail=f"There was a problem reading the daily events")
    return daily_events