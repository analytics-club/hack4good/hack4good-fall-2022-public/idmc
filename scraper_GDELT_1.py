import requests
from bs4 import BeautifulSoup
import zipfile
from io import BytesIO
import pandas as pd
import os
import re
import json

def get_article_url():
    """
    The function returns the links to the articles stored in the latest csv
    file hosted on GDELT data set 1.
    A single new csv file is uploaded every day at 6AM EST.
    """
    URL = 'http://data.gdeltproject.org/events/index.html'

# Collecting all article elements from the html of the URL
    try:
      page = requests.get(URL)
    except Exception as e:
      print("Could not reach website, URL might be invalid")
      print(e)
    soup = BeautifulSoup(page.content, "html.parser")
    res = soup.find_all("a")

# Removing first entries to have the latest csv file on top
    res = [v for i,v in enumerate(res) if i not in range(3)]
# Recovering the latest csv file
    latest = res[0]

# Downloading the zip file containing the latest csv file 
    zipFileURL = 'http://data.gdeltproject.org/events/' + latest["href"]
# Split URL to get the file name and remove .zip extension
    zip_name = zipFileURL.split('/')[-1]
    filename = os.path.splitext(zipFileURL.split('/')[-1])[0]

    try:
      req = requests.get(zipFileURL)
      open(zip_name, "wb").write(req.content)
    except Exception as e:
      print("Could not download the corresponding file from GDELT")
      print(e)

# Extracting the zip file contents
    with zipfile.ZipFile(zip_name,"r") as zip_ref:
        zip_ref.extractall()

# Recovering the list of url to the articles
    df = pd.read_csv(filename, sep='\t')
    article_url = df.iloc[:,-1].tolist()
    #article_location = df.iloc[:,-8].tolist()

# Deleting downloaded files
    os.remove(filename)
    os.remove(zip_name)

    return list(set(article_url))

if __name__ == "__main__":
    urls = get_article_url()
    with open('url.txt', 'w') as f:
        for line in urls:
            f.write(line)
            f.write('\n')
    with open('urls.json', 'w') as f:
        json.dump(urls, f, indent=4)
