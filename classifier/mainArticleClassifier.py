from articleClassifierDL import ArticleDiasterClassifier
from articleClassifierML import ArticleRelevanceClassifier
import pandas as pd


class MainArticleClassifier:
    def __init__(self):
        self.articleDiasterClassifier = ArticleDiasterClassifier()
        self.articleRelevanceClassifier = ArticleRelevanceClassifier()

    def classifyDocuments(self, documents):
        """
        documents: a list of text (e.g. title)
        return classified results (Unknown, Other, Conflict, Disaster)
        """
        if len(documents) == 0:
            return "Unknown"
        relevances = self.articleRelevanceClassifier.makePredictionML(documents)
        disasterTypes = self.articleDiasterClassifier.diasterTypeOfDocuments(documents)
        for i in range(len(relevances)):
            if relevances[i] == "Disaster" and disasterTypes[i] == "not a disaster":
                relevances[i] = "Other"
        return relevances, disasterTypes


if __name__ == "__main__":
    urls_with_title_text = pd.read_csv("../urls_with_title_text_en.csv", index_col=[0])
    urls_with_title_text = urls_with_title_text.fillna("")
    mainArticleClassifier = MainArticleClassifier()
    relevances, disasterTypes = mainArticleClassifier.classifyDocuments(urls_with_title_text["title"])
    urls_with_title_text["relevance"] = relevances
    urls_with_title_text["disasterType"] = disasterTypes
    print(urls_with_title_text)
    urls_with_title_text.to_csv("urls_with_classified.csv")
