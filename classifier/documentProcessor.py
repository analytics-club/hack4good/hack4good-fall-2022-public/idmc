import re
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords

stemmer = WordNetLemmatizer()


def XDataPreprocess(X_transform_fitter, X):
    '''
    X_transform_fitter and X are assumed to be a pd Series of text
    X_transform_fitter is used to fit the data transformer
    '''
    documents = []

    for sen in range(0, len(X_transform_fitter)):

        document = cleanDocument(X_transform_fitter[sen])
        documents.append(document)

    # The following script uses the bag of words model to convert text documents into corresponding numerical features:
    vectorizer = CountVectorizer(
        max_features=1500, min_df=5, max_df=0.7, stop_words=stopwords.words('english'))
    _ = vectorizer.fit_transform(documents)

    X = vectorizer.transform(X).toarray()

    # To convert values obtained using the bag of words model into TFIDF values, execute the following script:
    tfidfconverter = TfidfTransformer()
    X = tfidfconverter.fit_transform(X).toarray()
    return X


def cleanDocument(document):
    '''
    clean a document: removing punctuations, lemmatize, spaces etc. 
    '''

    # Remove all the special characters
    document = re.sub(r'\W', ' ', str(document))

    # remove all single characters
    document = re.sub(r'\s+[a-zA-Z]\s+', ' ', document)

    # Remove single characters from the start
    document = re.sub(r'\^[a-zA-Z]\s+', ' ', document)

    # Substituting multiple spaces with single space
    document = re.sub(r'\s+', ' ', document, flags=re.I)

    # Removing prefixed 'b'
    document = re.sub(r'^b\s+', '', document)

    # Converting to Lowercase
    document = document.lower()

    # Lemmatization
    document = document.split()

    document = [stemmer.lemmatize(word) for word in document]
    document = ' '.join(document)
    return document
