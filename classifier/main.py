import pandas as pd

from articleClassifierDL import ArticleClassifier


if __name__ == "__main__":
    urls_with_title_text = pd.read_csv("../urls_with_title_text_en.csv", index_col=[0])
    print(urls_with_title_text.columns)
    articleClassifier = ArticleClassifier()
    disasterTypes = []
    for title in urls_with_title_text["title"]:
        print(title, type(title))
        disasterType = ""
        if type(title) == str:
            disasterType = articleClassifier.disasterType(title)
        disasterTypes.append(disasterType)
    urls_with_title_text["disasterType"] = disasterTypes
    print(urls_with_title_text)
    urls_with_title_text.to_csv("urls_with_classified.csv")
