from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

import pandas as pd
from nltk.corpus import stopwords
import pickle
import numpy as np

import nltk
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

from documentProcessor import cleanDocument, XDataPreprocess

if __name__ == "__main__":

    nltk.download("stopwords")
    nltk.download("popular")

    print("--------loading data--------")
    # Prepare the X, y
    data = pd.read_excel("../Figures-Helix-H4G-IDMC.xlsx")
    X, y = data["Entry Title"], data["Figure cause"]

    X = XDataPreprocess(X, X)

    print("--------finished loading data--------")

    print("--------training-------")

    # Training and Testing Sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    classifier = RandomForestClassifier(n_estimators=1000, random_state=0, class_weight="balanced")
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)

    print("--------finished training-------")

    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    print(accuracy_score(y_test, y_pred))

    # Save Model
    with open("article_classifierML", "wb") as picklefile:
        pickle.dump(classifier, picklefile)
