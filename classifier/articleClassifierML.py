from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

import pandas as pd
from nltk.corpus import stopwords
import pickle
import numpy as np

import nltk
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

from documentProcessor import cleanDocument, XDataPreprocess

# xDataPreprocessor = pickle.load(open("xDataPreprocessor.p", "rb"))


class ArticleRelevanceClassifier:
    def __init__(self):
        # Load model
        with open("article_classifierML", "rb") as training_model:
            self.model = pickle.load(training_model)
        # Load data needed for preprocessing
        data = pd.read_excel("../Figures-Helix-H4G-IDMC.xlsx")
        self.X_for_fit_preprocessor = data["Entry Title"]

    def makePredictionML(self, X, needsPreprocessing=True):
        """
        X_transform_fitter and X are assumed to be a pd Series of text
        X_transform_fitter is used to fit the data transformer
        returns predicted results (disaster, conflict, other) given text
        """
        self.X = X
        if needsPreprocessing:
            self.X = self.dataPreprocess(self.X)
        y_pred = self.model.predict(self.X)
        print("len(y_pred): ", len(y_pred))
        return y_pred

    def dataPreprocess(self, X):
        return XDataPreprocess(X_transform_fitter=self.X_for_fit_preprocessor, X=X)


# def makePredictionML(X_transform_fitter, X):
#     """
#     X_transform_fitter and X are assumed to be a pd Series of text
#     X_transform_fitter is used to fit the data transformer
#     returns predicted results (disaster, conflict, other) given text
#     """
#     # Load model
#     with open("article_classifierML", "rb") as training_model:
#         model = pickle.load(training_model)

#     X = XDataPreprocess(X_transform_fitter, X)
#     y_pred = model.predict(X)
#     return y_pred


if __name__ == "__main__":
    # print('''This is a demo of the model. However, it uses the whole dataset
    #     (where 80% were used for training), so the result would be very biased.''')
    # data = pd.read_excel("../Figures-Helix-H4G-IDMC.xlsx")
    # X_transform_fitter, y = data["Entry Title"], data["Figure cause"]
    # y_pred = makePredictionML(X_transform_fitter, X_transform_fitter)
    # y_test = y
    # print(y[:3])
    # print(confusion_matrix(y_test, y_pred))
    # print(classification_report(y_test, y_pred))
    # print(accuracy_score(y_test, y_pred))

    # print("""---a real world example would be:---""")
    # examples = np.array(
    #     [
    #         "NBC: Evacuations Lifted in 1,100-Acre Brush Fire in Santa Clarita Valley",
    #         "KRQE News: Dog Head Fire: Information for evacuees",
    #         "France 24: Hurricane Fiona batters Turks and Caicos after devastating Puerto Rico - 21/09/2022",
    #         "Russia-Ukraine War Explosion Damages Crimea Bridge, Imperiling Russian Supply Route",
    #         "Arizona court halts enforcement of near-total abortion ban",
    #         "Wow, Google Really, Really Wants to Be Cooler Than Apple",
    #         "The Hack4Good coordinator is in charge of facilitating the communication between the H4G Organization Committee to address any organizational issue which might arise.",
    #     ]
    # )

    # y_pred = makePredictionML(X_transform_fitter, examples)
    # print(len(y_pred))
    # for i in range(len(examples)):
    #     print(f"{examples[i]}, ---> {y_pred[i]}")

    urls_with_title_text = pd.read_csv("../urls_with_title_text_en.csv", index_col=[0])
    urls_with_title_text = urls_with_title_text.fillna("")
    print(urls_with_title_text.columns)
    articleClassifier = ArticleRelevanceClassifier()
    relevance = articleClassifier.makePredictionML(urls_with_title_text["title"])
    for i, title in enumerate(urls_with_title_text["title"]):
        if title == "":
            relevance[i] = "Unknown"
    # for title in urls_with_title_text["title"]:
    #     print(title, type(title))
    #     disasterType = [""]
    #     if type(title) == str:
    #         disasterType = articleClassifier.makePredictionML([title])
    #     disasterTypes.append(disasterType)
    urls_with_title_text["relevance"] = relevance
    print(urls_with_title_text)
    urls_with_title_text.to_csv("urls_with_classified_ML.csv")
