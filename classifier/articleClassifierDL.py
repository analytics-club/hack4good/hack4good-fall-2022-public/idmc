import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import time
import pandas as pd
from typing import List


class IsDisasterClassifier:
    def __init__(self):
        self.tokenizer = AutoTokenizer.from_pretrained("sacculifer/dimbat_disaster_distilbert")
        self.model = AutoModelForSequenceClassification.from_pretrained(
            "sacculifer/dimbat_disaster_distilbert", from_tf=True
        )

    def isDisaster(self, text):
        try:
            inputs = self.tokenizer(text, return_tensors="pt")
            with torch.no_grad():
                logits = self.model(**inputs).logits
            predicted_class_id = logits.argmax().item()
            return {1: True, 0: False}[predicted_class_id]
        except Exception as e:
            print(text, e)
            return "Unknown"


class DisasterTypeClassifier:
    def __init__(self):
        self.tokenizer = AutoTokenizer.from_pretrained("sacculifer/dimbat_disaster_type_distilbert")
        self.model = AutoModelForSequenceClassification.from_pretrained(
            "sacculifer/dimbat_disaster_type_distilbert", from_tf=True
        )

    def disasterType(self, text):
        try:
            inputs = self.tokenizer(text, return_tensors="pt")
            with torch.no_grad():
                logits = self.model(**inputs).logits
            predicted_class_id = logits.argmax().item()
            return {
                1: "disease",
                2: "earthquake",
                3: "flood",
                4: "hurricane & tornado",
                5: "wildfire",
                6: "industrial accident",
                7: "societal crime",
                8: "transportation accident",
                9: "meteor crash",
                0: "haze",
            }[predicted_class_id]
        except Exception as e:
            print(text, e)
            return "Unknown"


class ArticleDiasterClassifier:
    def __init__(self):
        self.isDisasterClassifier = IsDisasterClassifier()
        self.disasterTypeClassifier = DisasterTypeClassifier()

    def isDisaster(self, text: str) -> bool:
        return self.isDisasterClassifier.isDisaster(text)

    def disasterType(self, text: str) -> str:
        if not self.isDisaster(text):
            return "not a disaster"
        return self.disasterTypeClassifier.disasterType(text)

    def diasterTypeOfDocuments(self, texts: List[str]) -> List[str]:
        disasterTypes = []
        for text in texts:
            disasterType = ""
            if type(text) == str and len(text) > 0:
                disasterType = self.disasterType(text)
            disasterTypes.append(disasterType)
        return disasterTypes


if __name__ == "__main__":

    # ac = ArticleClassifier()
    urls_with_title_text = pd.read_csv("../urls_with_title_text_en.csv", index_col=[0])
    urls_with_title_text.fillna("")
    articleClassifier = ArticleDiasterClassifier()
    disasterTypes = []
    for title in urls_with_title_text["title"]:
        print(title, type(title))
        disasterType = ""
        if type(title) == str:
            disasterType = articleClassifier.disasterType(title)
        disasterTypes.append(disasterType)
    urls_with_title_text["disasterType"] = disasterTypes
    print(urls_with_title_text)

    urls_with_title_text.to_csv("urls_with_classified.csv")

    print("--------------------------------------------------------------------------")
    examples = [
        "NBC: Evacuations Lifted in 1,100-Acre Brush Fire in Santa Clarita Valley",
        "KRQE News: Dog Head Fire: Information for evacuees",
        "France 24: Hurricane Fiona batters Turks and Caicos after devastating Puerto Rico - 21/09/2022",
        "Russia-Ukraine War Explosion Damages Crimea Bridge, Imperiling Russian Supply Route",
        "Arizona court halts enforcement of near-total abortion ban",
        "Wow, Google Really, Really Wants to Be Cooler Than Apple",
        "The Hack4Good coordinator is in charge of facilitating the communication between the H4G Organization Committee to address any organizational issue which might arise.",
    ]

    print("-----------------below is an example of this classifier--------------------")
    start = time.time()
    for example in examples:
        isDisaster = ac.isDisaster(example)
        disasterType = ac.disasterType(example)
        print(f"{example} \n isDisaster? {isDisaster}\n disasterType? {disasterType} \n\n")
    print(f"Time used for {len(examples)} examples: {time.time() - start} seconds")
