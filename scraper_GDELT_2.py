import requests
from bs4 import BeautifulSoup
import zipfile
from io import BytesIO
import pandas as pd
import os
import re
import translators as ts

def get_article_url2():
    """
    The function returns the links to the articles stored in the latest csv
    file hosted on GDELT data set 2.
    A new csv file is uploaded every 15 minutes.
    """
    URL = 'http://data.gdeltproject.org/gdeltv2/lastupdate-translation.txt'

# Collecting all article elements from the html of the URL
    try:
      links = requests.get(URL)
    except Exception as e:
      print("Could not reach website, URL might be invalid")
      print(e)

# Res is the url of the latest csv file uploaded in the past 15 minutes
#containing links to articles in over 65 languages
    myString = links.text
    res = re.search("(?P<url>https?://[^\s]+)", myString).group("url")

    zipFileURL = res

# Downloading the file by sending the request to the URL
    try:
      req = requests.get(zipFileURL)
    except Exception as e:
      print("Could not download the corresponding file from GDELT")
      print(e)

# extracting the zip file contents
    try:
      zip= zipfile.ZipFile(BytesIO(req.content))
      zip.extractall()
    except Exception as e:
      print("Could not extract zip file downloaded")
      print(e)

# Split URL to get the file name and remove .zip extension
    filename = os.path.splitext(zipFileURL.split('/')[-1])[0]

# Recovering the list of url to the articles
    df = pd.read_csv(filename, sep='\t')
    article_url = df.iloc[:,-1].tolist()

# Deleting downloaded files
    os.remove(filename)

    return list(set(article_url))

if __name__ == "__main__":
    urls = get_article_url2()
    with open('url.txt', 'w') as f:
        for line in urls:
            f.write(line)
            f.write('\n')
