import pandas as pd
import gensim
import geograpy
import json
import nltk
nltk.download('brown')
from nltk.corpus import brown
import newspaper
import wordtodigits

from newspaper import Article
from nltk.corpus import brown




""" Extract Title and Text"""

def extract_article_text(url):
   try:
    article= Article(url)
    article.download()
    article.parse()

    return article.title, article.text

   except Exception:
    return None, None

"""## Extract location"""

def extract_location(url):
  try: 
   places = geograpy.get_geoPlace_context(url=url)
   return places.country_mentions[0][0]
   
  except Exception:
   return None




""" Extract affected people or household"""

##Word2vector 
model = gensim.models.Word2Vec(brown.sents())

def Find_Displacement(url):

  ##extract text
  text=extract_article_text(url)[1]
  
  if text == None:
    return None
  
  else:

    ##split and convert the texts to list
    converted_str = [wordtodigits.convert(s) for s in text.split()]
    ##get indexes of number
    index_list = [s for s in range(len(converted_str)) if converted_str[s].isdigit()]

    ## empty list 
    figure_list= []
    ##check similarity
    for i in index_list:

      try: 
        if model.wv.similarity(converted_str[i+1],'building') >  0.85:
          figure_list.append(converted_str[i]+' '+converted_str[i+1])

        elif model.wv.similarity(converted_str[i+1],'people') >  0.85:
          figure_list.append(converted_str[i]+' '+converted_str[i+1])

      except Exception:
        pass

    
    return set(figure_list)




""" Update location and number of displacement"""


'''
with open(json_path, 'r+') as f:
    data = json.load(f)

    for obj in data:
      obj['state'] = extract_location(obj['url'])
      obj['numberDisplaced']= Find_Displacement(obj['url'])

    f.seek(0)
    json.dump(data, f, indent=4)
    f.truncate()
'''


df['state'] = df.apply(lambda x: extract_location(x['url']), axis=1)
df['numberDisplaced'] = df.apply(lambda x: Find_Displacement(x['url']), axis=1)

